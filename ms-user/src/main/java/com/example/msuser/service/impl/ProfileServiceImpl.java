package com.example.msuser.service.impl;


import com.example.msuser.exception.InvalidPasswordException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.example.msuser.dao.entity.User;
import com.example.msuser.dao.repository.UserRepository;
import com.example.msuser.exception.UserIsNotFound;

import static com.example.msuser.model.consts.OperationMessage.INVALID_PASSWORD;
import static com.example.msuser.model.consts.OperationMessage.USER_NOT_FOUND;

import com.example.msuser.model.consts.UpdatePassRequest;
import com.example.msuser.model.dto.UserDto;
import com.example.msuser.service.ProfileService;

import java.util.List;
import java.util.Locale;

import static com.example.msuser.builder.BuildUser.BUILD_USER;

@Service
@RequiredArgsConstructor
public class ProfileServiceImpl implements ProfileService {
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final MessageSource messageSource;

    @Override
    public UserDto getUserById(Long id) {
        Locale locale = LocaleContextHolder.getLocale();
        Object[] objs1 = new Object[1];
        objs1[0] = id;
        return BUILD_USER.buildDto(userRepository.findById(id)
                .orElseThrow(() ->
                        new UserIsNotFound(messageSource.getMessage(USER_NOT_FOUND.getMessage(), objs1, locale))));


    }

    @Override
    public List<UserDto> getAllUsers() {
        return BUILD_USER.buildDtoList(userRepository.findAll());
    }

    @Override
    public UserDto updateUser(Long id, User user) {
        User userFromDb = userRepository.findById(id).get();
        userFromDb.setName(user.getName());
        userFromDb.setSurname(user.getSurname());
        userFromDb.setUsername(user.getUsername());
        userFromDb.setEmail(user.getEmail());
        userFromDb.setJobTitle(user.getJobTitle());
        userFromDb.setRole(user.getRole());
        userRepository.save(userFromDb);
        return BUILD_USER.buildDto(userFromDb);

    }

    @Override
    public void updatePassword(Long id, UpdatePassRequest updatePassRequest) {
        Locale locale = LocaleContextHolder.getLocale();
        User user = userRepository.findById(id)
                .orElseThrow(() -> new UserIsNotFound("User with this " + id + " is not found"));
        if (passwordEncoder.matches(updatePassRequest.getOldPassword(), user.getPassword())){
            user.setPassword(passwordEncoder.encode(updatePassRequest.getNewPassword()));
            userRepository.save(user);
        }else {
            throw new InvalidPasswordException(messageSource.getMessage(INVALID_PASSWORD.getMessage(), null,locale));

        }


    }
}