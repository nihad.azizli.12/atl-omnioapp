package com.example.msuser.service;

import com.example.msuser.dao.entity.User;
import com.example.msuser.model.consts.UpdatePassRequest;
import com.example.msuser.model.dto.UserDto;

import java.util.List;

public interface ProfileService {
    UserDto getUserById(Long id);

    List<UserDto> getAllUsers();

    UserDto updateUser(Long id, User user);

    void updatePassword(Long id, UpdatePassRequest updatePassRequest);
}
