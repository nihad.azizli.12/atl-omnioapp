package com.example.msuser.exception.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.example.msuser.exception.InvalidPasswordException;
import com.example.msuser.exception.UserIsNotFound;

@RestController
@Slf4j
public class GlobalExceptionHandler {
    @ExceptionHandler(UserIsNotFound.class)
    public ErrorResponse handle(UserIsNotFound exception) {
        log.error("ClientException: ", exception);
        return ErrorResponse.of(exception);
    }
    @ExceptionHandler(InvalidPasswordException.class)
    public ErrorResponse handle(InvalidPasswordException exception) {
        log.error("ClientException: ", exception);
        return ErrorResponse.of(exception);
    }


}
