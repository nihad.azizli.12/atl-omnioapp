package com.example.msuser.exception;

public class UserIsNotFound extends RuntimeException {
    public UserIsNotFound(String message) {
        super(message);
    }
}