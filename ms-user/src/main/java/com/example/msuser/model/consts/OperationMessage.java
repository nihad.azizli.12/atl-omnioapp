package com.example.msuser.model.consts;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum OperationMessage {
    USER_NOT_FOUND("userIs.notFound"),
    INVALID_PASSWORD("invalid.Password");
    private final String message;
}
