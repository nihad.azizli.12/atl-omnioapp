package com.example.msuser.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)

public class RegisterRequest {
    String name;
    String surname;
    String username;
    String email;
    String jobTitle;

    String password;
    Role role;

}
