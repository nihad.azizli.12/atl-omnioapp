package com.example.msuser.model.dto;


import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;
import lombok.experimental.FieldDefaults;
import com.example.msuser.model.Role;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserDto {

    String name;
    String surname;
    String username;
    String email;
    String jobTitle;
    @Enumerated(EnumType.STRING)
    Role role;
}
