package com.example.msuser.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import com.example.msuser.dao.entity.User;
import com.example.msuser.model.consts.UpdatePassRequest;
import com.example.msuser.model.dto.UserDto;
import com.example.msuser.service.ProfileService;

import java.util.List;


@RestController
@RequestMapping("/profile")
@RequiredArgsConstructor
public class ProfileController {
    private final ProfileService profileService;
    @GetMapping("/get-user-by-id/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDto getUserProfile(@PathVariable Long id){
        return profileService.getUserById(id);

    }
    @GetMapping("/get-all-users")
    @ResponseStatus(HttpStatus.OK)
    public List<UserDto> getAllUsers(){
        return profileService.getAllUsers();

    }
    @PutMapping("/update-user/{id}")
    public UserDto updateUser(@PathVariable Long id, @RequestBody User user){
        return profileService.updateUser(id,user);

    }
    @PutMapping("/update-pass/{id}")
    public void updatePassword(@PathVariable Long id, @RequestBody UpdatePassRequest updatePassRequest){
        profileService.updatePassword(id,updatePassRequest);



    }

}
